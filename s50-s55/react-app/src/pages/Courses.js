import coursesData from "../Data/Courses"
import CourseCard from "../components/CourseCard"
import { useEffect, useState } from "react"



export default function Courses(){
	// Checks to see if mock data was captured
	/*console.log(coursesData);
	console.log(coursesData[0])*/

	// State that will be used to store courses retrieved from dfatabase

	const [Allcourses, setAllCourses] = useState([]);

	// Retrieves the courses from database upon initial render of the "Courses" components

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json()).then(data => {
			console.log(data);


			setAllCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course}/>
					)
			}))


		})
	}, [])



	return(
		<>
			<h1>Courses</h1>
			{/*Prop making and prop passing*/}
			{Allcourses}
		</>
		)
}