// DOM => Document Object Manipulation

// CSS we are using "CLASS" to target the data in JAVASCRIPT we use "ID" to target data

const txtFirstName = document.getElementById("txt-first-name");
const spanFullName = document.getElementById("span-full-name");

txtFirstName.addEventListener("keyup", (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener("keyup", (event) => {
	console.log(event.target);
	console.log(event.target.value);
})